#include"compass.h"
using namespace std;




comp::comp(){
    fd = wiringPiI2CSetup (addr);
    x_low = y_low = x_high = y_high = 0;
}



void comp::init(int mode, int odr, int rng, int osr){
    u_int8_t data = (mode | (odr << 2) | (rng << 4) | (osr << 6));

    reset();
    wiringPiI2CWriteReg8 (fd, comp_control1, data);
    wiringPiI2CWriteReg8 (fd, comp_control2, 0);		//Funktion des CR2 nicht definded
    read_data();
    x_low = x_high = x;
    y_low = y_high = y;
}

void comp::read_data(){
	//wait for new data
	if(multi_measure > 0)
	{
		int xtemp, ytemp, ztemp, ttemp;
		xtemp = ytemp = ztemp = ttemp = 0;
		for(int i = 0;i < multi_measure ; i++)
		{
            while(!(wiringPiI2CReadReg8(fd, comp_state) & drdy)){
                delay(3);//cpu Entlasstung
			}/*
                xtemp += wiringPiI2CReadReg16 (fd, comp_x);
                ytemp += wiringPiI2CReadReg16 (fd, comp_y);
                ztemp += wiringPiI2CReadReg16 (fd, comp_z);
                ttemp += wiringPiI2CReadReg16 (fd, comp_t);*/
                xtemp += wiringPiI2CReadReg8 (fd, 0) | (wiringPiI2CReadReg8 (fd, 1) << 8 );
                ytemp += wiringPiI2CReadReg8 (fd, 2) | (wiringPiI2CReadReg8 (fd, 3) << 8 );
                ztemp += wiringPiI2CReadReg8 (fd, 4) | (wiringPiI2CReadReg8 (fd, 5) << 8 );
                ttemp += wiringPiI2CReadReg8 (fd, 6) | (wiringPiI2CReadReg8 (fd, 7) << 8 );
		}
		x = xtemp / multi_measure;
		y = ytemp / multi_measure;
		z = ztemp / multi_measure;
		t = ttemp / multi_measure;
		return;
	}

    while(!(wiringPiI2CReadReg8(fd, comp_state) & drdy)){
        delay(3);//cpu Entlasstung
    }

    x = wiringPiI2CReadReg8 (fd, 0) | (wiringPiI2CReadReg8 (fd, 1) << 8 );
    y = wiringPiI2CReadReg8 (fd, 2) | (wiringPiI2CReadReg8 (fd, 3) << 8 );
    z = wiringPiI2CReadReg8 (fd, 4) | (wiringPiI2CReadReg8 (fd, 5) << 8 );
    t = wiringPiI2CReadReg8 (fd, 6) | (wiringPiI2CReadReg8 (fd, 7) << 8 );
}

void comp::measure(){
    read_data();

    //High/Low anpassen
    if(x < x_low) { x_low = x; }
    if(x > x_high){	x_high = x; }
    if(y < y_low) {	y_low = y; }
    if(y > y_high){	y_high = y; }

    //0-Division verhinder
    if( x_low==x_high || y_low==y_high ) 
        return;

    //auf 0-Punkt zentrieren
    x_cen = x - (x_high + x_low)/2;
    y_cen = y - (y_high + y_low)/2;

    //scaling anpassen
    float fx = (float)x_cen/(x_high-x_low);
    float fy = (float)y_cen/(y_high-y_low);

    //Winkel ausrechnen
    heading = 180.0 * atan2(fy, fx) / 3.14159;
    if(heading<=0) heading += 360;

    //cout << "comp_head :" << heading << "\t x:" << x << "\t y:" << y << "\t xcen:" << x_cen << "\t ycen:" << y_cen  << endl;

}


