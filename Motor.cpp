#include "adafruit-motor-hat-cpp-library/source/adafruitmotorhat.h"
#include <thread>
#include <chrono>
#include"compass.h"


#define left	0
#define right	1

using namespace std;

/*
void straight(auto motor1, auto motor2, int speed, int time){
    for(int i = 0; i < time; i++){
        motor1->setSpeed (speed);
        motor2->setSpeed (speed);
        motor1->run (AdafruitDCMotor::kForward);
        motor2->run (AdafruitDCMotor::kForward);
        std::this_thread::sleep_for (0.1s);
        motor1->run (AdafruitDCMotor::kRelease);
        motor2->run (AdafruitDCMotor::kRelease);
    }
}
*/

void straight(AdafruitMotorHAT *hat, int time){
	hat->getMotor(1)->run (AdafruitDCMotor::kForward);
	hat->getMotor(2)->run (AdafruitDCMotor::kForward);
	this_thread::sleep_for (chrono::seconds(time));
	hat->getMotor(1)->run (AdafruitDCMotor::kRelease);
	hat->getMotor(2)->run (AdafruitDCMotor::kRelease);
}

void turn(AdafruitMotorHAT *hat, int dir){
	if(dir == left)
		hat->getMotor(2)->run (AdafruitDCMotor::kForward);
	else
		hat->getMotor(1)->run (AdafruitDCMotor::kForward);

	this_thread::sleep_for (0.1s);

	hat->getMotor(1)->run (AdafruitDCMotor::kRelease);
	hat->getMotor(2)->run (AdafruitDCMotor::kRelease);

}

/*
void turn_90_Degree(auto motor1){
    motor1->setSpeed(75);
    motor1->run (AdafruitDCMotor::kForward);
    this_thread::sleep_for (2s);
    motor1->run (AdafruitDCMotor::kRelease);
}*/
/*
void Init_Motor(AdafruitMotorHAT hat, auto &motor1, auto &motor2){
    if (auto motor1 { hat.getMotor (1) })
    {
        cout << "Motor1 Init Done" << endl;
        if(auto motor2 { hat.getMotor (2)}){
            cout << "Motor2 Init Done" << endl;
        }
    }
}
*/
void turn_comp(AdafruitMotorHAT *hat, comp *k, int degree){
	int start_heading = k->get_heading();
	while(true){
		if(degree > 0)
			turn(hat, left);
		else
			turn(hat, right);

		if(start_heading + degree == k->get_heading())
			return;
	}
}

void straight_comp(AdafruitMotorHAT *hat, comp *k, int time){
	int start_heading = k->get_heading();

	for(int t = 0; t < time; t+=0.1 )
	{
		straight(hat, 0.1);
		if( k->get_heading() != start_heading)
			turn_comp(hat, k, k->get_heading()-start_heading);
	}
}



