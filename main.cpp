///
/// Project main
///

/// Includes
#include <signal.h>
#include <wiringPi.h>
#include "Motor.cpp"
#include <iostream>
#include "HC-SR04_V1.cpp"
#include "compass.h"
#include <thread>
using namespace std;

bool stoping = false;
/// Interrupt Routine for STRG-C
void signalHandler(int signum)
{
    std::cout << "Strg-C Programmende" << std::endl;
	// Beenden Sie hier bitte alle Verbindung zu den Sensoren etc.
    stoping = true;
    //exit(hat);
    //exit(compass_thread);
    exit(signum);
}


void countinues_measure(comp *k){
	while(!stoping){
		k->measure();
	}
}

void test_motor(AdafruitMotorHAT *hat, comp *k){
	cout << "1s left turn" << endl;
	for(int i = 0;i < 10;i++) turn(hat, left);//1s left turn
	cout << "1s right turn" << endl;
	for(int i = 0;i < 10;i++) turn(hat, right);//1s right turn^
    //cout << "5s straight"<< endl;
	//straight(hat,1);//1s straight
    cout << "Drive till find a Obstacle" << endl;
    while(is_there_a_Obstacle()){
        straight(hat,1);
        cout << ".";
    }
	//turn_comp(&hat,k,90);//90° right turn
	//turn_comp(hat,k,-90);//90° left turn
	//turn_comp(hat,k,45);//45° right turn
	//turn_comp(hat,k,-120);//120° left turn

	//straight_comp(hat, k, 5s)//5s straight with controll
}



int main(){
	// Csignal für Abbruch über STRG-C
	signal(SIGINT, signalHandler);

    wiringPiSetup ();
    Init_ultrasonic_sensor();
	//Kompass start
	comp k;
	k.init(continuous, odr200, rng8, osr512);
    k.set_multi_measure(5);
	thread compass_thread (countinues_measure, &k);

	//Motor start

    AdafruitMotorHAT hat;
    hat.getMotor(1)->setSpeed(125);
    hat.getMotor(2)->setSpeed(125);

    //Ultra start


    test_motor(&hat, &k );

    while(!stoping){
        cout << "heading"<< k.get_heading() << endl;
        delay(500);
    }
    //Init_ultrasonic_sensor(0,1);

 // connect using the default device address 0x60
    //Init_Motor(hat, &motor1, &motor2);

    //hile(is_there_a_Obstacle(0,1)){
    //    straight(motor1, motor2, 125, 1);
    //};



    /*for(int i = 0; i < 6; i++){
        turn_90_Degree(motor1);
    }*/


    //compass_thread.join();

    return 0;
}
