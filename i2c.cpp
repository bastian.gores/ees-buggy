#include <wiringPiI2C.h>
#include <iostream>
using namespace std;

//To test the I2C interface boards you first need to make sure the Linux kernel I2C modules are loaded. 
//Use wiringPi’s gpio command to load the modules if they are not auto-loaded.
void test_i2c(){
    int r = 0;
    r = wiringPiI2CSetup (13);
    cout << r << endl;
}
