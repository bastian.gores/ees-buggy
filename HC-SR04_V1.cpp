// Erkkenung eines Gegenstandes
// Gegenstand erkannt: return false 
// Wenn nein, return true
bool is_there_a_Obstacle(){
    // Parameter for Distance
    int trigger = 0; 
    int echo = 1;
    int cm = 0;

    for (int i = 0; i < 4; i++){
        // Set Tigger pin to high for 10us to start ultrasonic sensor 
        cout << "Messung:"  << i << endl;
        digitalWrite(trigger, HIGH);
        delayMicroseconds(10);
        digitalWrite(trigger, LOW);
        // Wait for Sensor to start Measurement
        while (digitalRead(echo) == LOW);
        while(digitalRead(echo) && (cm < 400)){
            delayMicroseconds(58);
            cm++;
        }
        cout << "cm:" << "\t" << cm << endl;
    }
    delayMicroseconds(10000);
    if((cm/3) < 5 )
        return false;
    return true;
}

void Init_ultrasonic_sensor(){
    //--- INIT PINOUT
    int trigger = 0; 
    int echo = 1;
    pinMode(trigger,OUTPUT);
    pinMode(echo, INPUT);
    digitalWrite(trigger, LOW);
}







