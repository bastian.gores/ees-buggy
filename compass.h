#ifndef compass_driver_h_
#define compass_driver_h_


#include <iostream>
#include <math.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>


#define addr			0x0D

//Register
#define comp_x			0x00
#define comp_y			0x02
#define comp_z			0x04
#define comp_state		0x06
#define comp_t			0x07
#define comp_control1	0x09
#define comp_control2	0x0A
#define comp_reset		0x0B

//Mode
#define standby			0
#define continuous		1

//Output Data Rate
#define odr10			0b00
#define odr50			0b01
#define odr100			0b10
#define odr200			0b11

//Scale
#define rng2			0
#define rng8			1

//Over Sample Ratio
#define osr512			0b00
#define osr256			0b01
#define osr128			0b10
#define osr64			0b11

//Statregister
#define drdy			0b1
#define ovl				0b10
#define dor				0b100


class comp {
    public:
		//Contructor
        comp();

        //Getter/Setter
        int get_fd(){return fd;};
        int get_x(){return x;};
        int get_y(){return y;};
        int get_z(){return z;};
        int get_t(){return t;};
        int get_heading(){return heading;};
        int get_x_low(){return x_low;};
        int get_y_low(){return y_low;};
        int get_x_high(){return x_high;};
        int get_y_high(){return y_high;};
        int get_x_cen(){return x_cen;};
        int get_y_cen(){return y_cen;};
        void set_multi_measure(int m) {multi_measure = m;};

        //Functions
        void reset(){wiringPiI2CWriteReg8 (fd, comp_reset, 1);};
        void init(int mode, int odr, int rng, int osr);
        void read_data();
        void measure();
        
    private:
        int multi_measure = 0;
        u_int8_t fd;
        int x,y,z,t;
        int x_low, y_low, x_high, y_high;
        int x_cen, y_cen;
        int heading = 0;
};

#endif
